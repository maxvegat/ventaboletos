const btnCalcular = document.getElementById('btnCalcular');

btnCalcular.addEventListener("click", function(){

   //obtener los valores de los inputs text

   let numBoleto = document.getElementById('numBoleto').value;
   let nombreC = document.getElementById('nombreC').value;
   let destino = document.getElementById('destino').value;
   let tipo = parseInt(document.getElementById('tipo').value);
   let precio = parseFloat(document.getElementById('precio').value);
   
   //hacer calculos
   let subtotal = precio;

   if (tipo === 2) {
       subtotal = precio + precio * 0.8;
   }

   let impuesto = precio * 0.16;
   let totalFinal = parseInt(subtotal) + parseInt(impuesto);

   //mostrar los resultados

   document.getElementById('subtotal').value = subtotal;
   document.getElementById('impuesto').value = impuesto;
   document.getElementById('totalFinal').value = totalFinal;

});